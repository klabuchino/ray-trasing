﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    struct Material
    {
        public double Specular { get; set; }
        public double Reflective { get; set; }
        public double Refraction { get; set; }
        public double Transparency { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="specular">отражение света. -1 = не отражает совсем</param>
        /// <param name="reflective">зеркальность</param>
        public Material(double specular, double reflective, double refraction, double transparency)
        {
            Specular = specular;
            Reflective = reflective;
            Refraction = refraction;
            Transparency = transparency;
        }

        public static Material plastic()
        {
            return new Material(10, 0, 0 , 0);
        }

        public static Material metal() 
        {
            return new Material(100, 0.5, 0 , 0);
        }
        public static Material glass()
        {
            return new Material(100, 0, 1.23, 0.7);
        }
    }

}

