﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ray_trasing
{
    class Program
    {
        static void Main()
        {
            var tracer = new Tracer(0.1);
            var camera = new Camera();
            
            var rand = new Random();
            tracer.shapes.Add(new Sphere(new Vector3(-0, 0, -1), 0.5, new Vector3(205, 127, 50), Material.metal()));
            tracer.shapes.Add(new Sphere(new Vector3(1.2, 0, -1), 0.2, new Vector3(255, 0, 0), Material.plastic()));
            tracer.shapes.Add(new Sphere(new Vector3(-1.2, 0, -1), 0.3, new Vector3(255, 255, 255), Material.glass()));
            tracer.shapes.Add(new Sphere(new Vector3(-3, 0, -3), 0.3, new Vector3(255, 0, 0), Material.plastic()));
            tracer.shapes.Add(new Sphere(new Vector3(0, -100.5, -1), 100, new Vector3(0, 255, 255),Material.plastic()));
            tracer.lights.Add(new Light(new Vector3(0, 2, -1), 0.3));
            tracer.lights.Add(new Light(new Vector3(-6, 2, -1), 0.6));
            StreamWriter writer = new StreamWriter("picture.ppm", false);
            writer.WriteLine("P3");
            writer.WriteLine("{0} {1} \n255", camera.Width, camera.Height);

            for (int y = camera.Height - 1; y >= 0; y--)
            {
                for (int x = 0; x < camera.Width; x++)
                {
                    var color = new Vector3();
                    for (int s = 0; s < camera.NumSumples; s++)
                    {
                        var ray = camera.GetRay(x + rand.NextDouble(), y + rand.NextDouble());
                        color += tracer.Trace(ray, 0);

                    }
                    color /= camera.NumSumples;
                    color.CheckColorOverflow();
                    writer.WriteLine("{0} {1} {2}", (int)(color.X), (int)(color.Y), (int)(color.Z));
                }
            }

            writer.Close();
        }
    }
}
