﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    class Tracer
    {
        public List<IShape> shapes;
        public List<Light> lights;
        public double AmbientLight { get; set; }
        public Tracer(double ambientLight)
        {
            shapes = new List<IShape>();
            lights = new List<Light>();
            AmbientLight = ambientLight;
        }

        public Vector3 Trace(Ray ray, int k)
        {
            var backgroundColor = new Vector3(255, 255, 255);
            var hit = HitedShapes(ray);
            if (!hit.IsHit) return backgroundColor;

            var localColor = hit.Color * ComputeLighting(hit, -ray.Direction);
            if (k < 3 && hit.Material.Reflective > 0)
            {
                var reflectedR = GetReflectedRay(ray, hit);
                localColor += Trace(reflectedR, k++) * hit.Material.Reflective;
            }
            if(k < 3 && hit.Material.Transparency > 0)
            {
                var refratedR = GetRefractedRay(ray, hit);
                localColor = localColor * (1 - hit.Material.Transparency) + Trace(refratedR, k++) * hit.Material.Transparency;
            }
            return localColor;
        }
        private Hit HitedShapes(Ray ray)
        {
            Hit trueHit = new Hit
            {
                IsHit = false
            };
            double nearest = double.MaxValue;
            foreach (var shape in shapes)
            {
                Hit hit = shape.CheckHit(ray, nearest);
                if (hit.IsHit)
                {
                    trueHit = hit;
                    nearest = hit.T;
                }
            }
            return trueHit;
        }

        private Ray GetReflectedRay(Ray ray, Hit hit)
        {
            Vector3 reflected = ray.Direction - 2 * hit.Normal * Vector3.ScalarMultiply(hit.Normal, ray.Direction);
            return new Ray(hit.Point, reflected);
        }

        private Ray GetRefractedRay(Ray ray, Hit hit)
        {
            var k = Math.Pow(hit.Material.Refraction, 2);
            var sqrLenIn = Math.Pow(/*ray.Direction.Length*/hit.Point.Length, 2);
            var sqrNDotIn = Math.Pow(Vector3.ScalarMultiply(hit.Normal, /*ray.Direction*/hit.Point), 2);
            var knormal = (Math.Sqrt((((k - 1) * sqrLenIn) / sqrNDotIn) + 1) - 1) * Vector3.ScalarMultiply(hit.Normal, hit.Point/*ray.Direction*/);
            Vector3 refracted = Vector3.ToUnitVector(/*ray.Direction*/hit.Point + (knormal * hit.Normal));
            return new Ray(hit.Point, refracted);
        }

        private double ComputeLighting(Hit hit, Vector3 dirToCamera)
        {
            double I = AmbientLight;
            foreach(var light in lights)
            {
                var direction = light.Position - hit.Point;
                var lightRay = new Ray(hit.Point, Vector3.ToUnitVector(direction));
                var lightHit = HitedShapes(lightRay);
                var intensity = light.Intensity;
                if(lightHit.Material.Transparency > 0)
                {
                    lightHit.IsHit = false;
                    intensity = light.Intensity * lightHit.Material.Transparency;
                }
                var DdotN = Vector3.ScalarMultiply(hit.Normal, direction);
                if (DdotN > 0 && !lightHit.IsHit)
                {
                    I += intensity * DdotN / (hit.Normal.Length * direction.Length);
                }
                if (hit.Material.Specular > -1 && !lightHit.IsHit)
                {
                    var reflected = 2 * hit.Normal * Vector3.ScalarMultiply(hit.Normal, direction) - direction;
                    var refDotD = Vector3.ScalarMultiply(reflected, dirToCamera);
                    if(refDotD > 0)
                    {
                        I += intensity * Math.Pow(refDotD / (reflected.Length * dirToCamera.Length), hit.Material.Specular);
                    }
                }
            }
            return I;
        }
    }
}