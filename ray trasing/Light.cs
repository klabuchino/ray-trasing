﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    struct Light
    {
        public Vector3 Position { get; set; }
        public double Intensity { get; set; }
        public Light(Vector3 position, double intensity)
        {
            Position = position;
            Intensity = intensity;
        }
    }
}
