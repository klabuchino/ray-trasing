﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    class Camera
    {
        public Vector3 Position { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Vector3 Horizontal { get; set; }
        public Vector3 Vertical { get; set; }
        public Vector3 LoverLeftCorner { get; set; }
        public int NumSumples { get; set; }

        public Camera()
        {
            Position = new Vector3(0, 0, 0);
            Width = 600;
            Height = 300;
            NumSumples = 100;
            Horizontal = new Vector3(6.0, 0, 0);
            Vertical = new Vector3(0, 3.0, 0);
            LoverLeftCorner = new Vector3(-3.0, -1.5, -1.0);           
        }

        public Ray GetRay(double x, double y)
        {
            
            double u = x / (double)Width;
            double v = y / (double)Height;
            Vector3 direction = Vector3.ToUnitVector(LoverLeftCorner + u * Horizontal + v * Vertical);
            return new Ray(Position, direction);
        }
    }
}
