﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    interface IShape
    {
        Hit CheckHit(Ray ray, double maxT);
    }
}
