﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    class Sphere: IShape
    {
        public Vector3 Center { get; set; }
        public double Radius { get; set; }

        public Vector3 Color { get; set; }

        public Material Material { get; set; }

        public Sphere(Vector3 center, double radius, Vector3 color, Material material)
        {
            Center = center;
            Radius = radius;
            Color = color;
            Material = material;
        }
        public Hit CheckHit(Ray ray, double maxT)
        {
            Hit hit = new Hit
            {
                IsHit = CalcDiscriminant(ray, out double t1, out double t2)
            };
            if (hit.IsHit)
            {
                if (t1 > 0.001 && t1 < maxT)
                {
                    hit.T = t1;
                }
                else if (t2 > 0.001 && t2 < maxT)
                {
                    hit.T = t2;
                }
                else hit.IsHit = false;
                hit.Point = ray.AtParameter(hit.T);
                hit.Normal = Vector3.ToUnitVector((hit.Point - Center) / Radius);
                hit.Color = Color;
                hit.Material = Material;
            }
            return hit;
        }

        private bool CalcDiscriminant(Ray ray, out double t1, out double t2)
        {
            //vector has unit length becouse A = 1;
            double B = 2.0 * Vector3.ScalarMultiply((ray.Origin - Center), ray.Direction);
            double C = Vector3.ScalarMultiply(ray.Origin - Center, ray.Origin - Center) - Radius * Radius;
            var discriminant =  B * B - 4 * C;
            t1 = double.NaN;
            t2 = double.NaN;
            if (discriminant > 0)
            {
                t1 = (-B - Math.Sqrt(discriminant)) / 2;
                t2 = (-B + Math.Sqrt(discriminant)) / 2;
                return true;
            }
            return false;
        }
    }
}
