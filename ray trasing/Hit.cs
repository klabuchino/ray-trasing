﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    /// <summary>
    /// scene intersection data
    /// </summary>
    struct Hit
    {
        public double T { get; set; }
        public Vector3 Point { get; set; }
        public Vector3 Normal { get; set; }
        public bool IsHit { get; set; }
        public Vector3 Color { get; set; }

        public Material Material { get; set; }
    }
}
