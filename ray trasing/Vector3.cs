﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ray_trasing
{
    class Vector3
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Vector3()
        {
            X = 0;
            Y = 0;
            Z = 0;
        }
        public Vector3(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector3 operator +(Vector3 a, Vector3 b)
        {
             return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector3 operator -(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }
        public static Vector3 operator -(Vector3 a)
        {
            return new Vector3(-a.X, -a.Y, -a.Z);
        }
        /// <summary>
        /// умножение вектора на скаляр
        /// </summary>
        /// <param name="a">вектор</param>
        /// <param name="num">скаляр</param>
        /// <returns></returns>
        public static Vector3 operator *(Vector3 a, double num)
        {
            return new Vector3(a.X * num, a.Y * num, a.Z * num);
        }

        public static Vector3 operator *(double num, Vector3 a)
        {
            return new Vector3(a.X * num, a.Y * num, a.Z * num);
        }
        public static Vector3 operator *(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
        }

        public static double ScalarMultiply (Vector3 a, Vector3 b)
        {
            return ((a.X * b.X) + (a.Y * b.Y) + (a.Z * b.Z));
        }

        public static Vector3 operator /(Vector3 a, double num)
        {
            return new Vector3(a.X / num, a.Y / num, a.Z / num);
        }

        public static Vector3 Sqrt(Vector3 a)
        {
            return new Vector3(Math.Sqrt(a.X), Math.Sqrt(a.Y), Math.Sqrt(a.Z));
        }

        public double Length { get { return Math.Sqrt(X * X + Y * Y + Z * Z); } }
        public static Vector3 ToUnitVector(Vector3 vector)
        {
            return vector / vector.Length;
        }

        public void CheckColorOverflow()
        {
            if (X > 255) X = 255;
            if (Y > 255) Y = 255;
            if (Z > 255) Z = 255;
            if (X < 0) X = 0;
            if (Y < 0) Y = 0;
            if (Z < 0) Z = 0;
        }
    }
}
